
package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import javax.transaction.Transactional;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VirementRepositoryTest {

  @Autowired
  private VirementRepository virementRepository;

  @Test
  public void findOne() {
	 
	  Virement v1= new Virement();
	    entityManager.persist(v1);

	    Virement v2= new Virement();
	    entityManager.persist(v2);
	    Virement foundVirement = VirementRepository.findOne(v2.getId()).get();

	    assertThat(foundVirement).isEqualTo(v2);
  }

  @Test
  public void findAll() {
	  Virement v1= new Virement();
	    entityManager.persist(v1);

	    Virement v2= new Virement();
	    entityManager.persist(v2);

	    Virement v3= new Virement();
	    entityManager.persist(v3);

	    Iterable<Virement> virements = Virementrepository.findAll();

	    assertThat(virements).hasSize(3).contains(v1, v2, v3);
  }

  @Test
  public void save() {
	  
	  Virement v1 =Virementrepository.save(new Virement());

	    assertThat(v1).hasFieldOrPropertyWithValue("compte1", "RIP1");
	    assertThat(v1).hasFieldOrPropertyWithValue();
  }

  @Test
  public void delete() {
	  
	  Virement v1= new Virement();
	    entityManager.persist(v1);

	    Virement v2= new Virement();
	    entityManager.persist(v2);

	    Virement v3= new Virement();
	    entityManager.persist(v3);
	    
	    Virementrepository.delete(v2.getId());

	    Iterable<Virement> Virements = virementrepository.findAll();

	    assertThat(Virements).hasSize(2).contains(v1, v3);
  }
}