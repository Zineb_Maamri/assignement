
package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import javax.transaction.Transactional;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VersementRepositoryTest {

  @Autowired
  private VersementRepository versementRepository;

  @Test
  public void findOne() {
	 
	  Versement v1= new Versement();
	    entityManager.persist(v1);

	    Versement v2= new Versement();
	    entityManager.persist(v2);
	    Versement foundVersement = VersementRepository.findOne(v2.getId()).get();

	    assertThat(foundVersement).isEqualTo(v2);
  }

  @Test
  public void findAll() {
	    Versement v1= new Versement();
	    entityManager.persist(v1);

	    Versement v2= new Versement();
	    entityManager.persist(v2);

	    Versement v3= new Versement();
	    entityManager.persist(v3);

	    Iterable<Versement> versements = Versementrepository.findAll();

	    assertThat(versements).hasSize(3).contains(v1, v2, v3);
  }

  @Test
  public void save() {
	  
	  Versement v1 = Versementrepository.save(new Versement());

	    assertThat(v1).hasFieldOrPropertyWithValue("compte1", "RIP1");
	    assertThat(v1).hasFieldOrPropertyWithValue();
  }

  @Test
  public void delete() {
	  
	  Versement v1= new Versement();
	    entityManager.persist(v1);

	    Versement v2= new Versement();
	    entityManager.persist(v2);

	    Versement v3= new Versement();
	    entityManager.persist(v3);
	    
	    Versementrepository.delete(v2.getId());

	    Iterable<Versement> Versements = versementrepository.findAll();

	    assertThat(Versements).hasSize(2).contains(v1, v3);
  }
}