package ma.octo.assignement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ma.octo.assignement.domain.AuditVersement;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface AuditVersementRepository extends JpaRepository<AuditVersement, Long> {
}

