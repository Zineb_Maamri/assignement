package ma.octo.assignement.repository;

import ma.octo.assignement.domain.AuditVirement;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface AuditVirementRepository extends JpaRepository<AuditVirement, Long> {
	save(AuditVirement a);
}
