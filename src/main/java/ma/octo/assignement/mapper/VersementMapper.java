package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;


public class VersementMapper {

    private static VersementDto versementDto;

    public static VersementDto map(Versement versement) {
    	versementDto = new VersementDto();
    	versementDto.setNrCompteEmetteur(versement.getCompteBeneficiaire().getNrCompte()); //getCompteEmetteur().getNrCompte())
    	versementDto.setDate(versement.getDateExecution());
    	versementDto.setMotif(versement.getMotifVersement());

        return versementDto;

    }
}
